import builder.TaskBuilder;
import executor.Executor;
import executor.MongoExecutor;
import model.field.*;
import model.operations.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import static model.field.Condition.Types.*;

public class Test {
    public static void main(String[] args) {
        Logger mongoLogger = Logger.getLogger( "org.mongodb.driver" );
        mongoLogger.setLevel(Level.SEVERE);


        TaskBuilder taskBuilder = new TaskBuilder();

        // INSERT EXAMPLE
        Operation insert = taskBuilder.createQueryOn("test", "test")
                                            .insertItems()
                                                .item(
                                                        Field.simple("_id", 1),
                                                        Field.simple("producto", "libro"),
                                                        Field.simple("autor", "Cervantes"),
                                                        Field.simple("precio", 40))
                                                .item(
                                                        Field.simple("_id", 4),
                                                        Field.simple("producto", "audiolibro"),
                                                        Field.simple("autor", "Cervantes"),
                                                        Field.simple("precio", 50))
                                                .item(
                                                        Field.simple("_id", 2),
                                                        Field.simple("producto", "audiolibro"),
                                                        Field.simple("autor", "Quevedo"),
                                                        Field.simple("precio", 30),
                                                        Field.compound("medidas",
                                                                Field.simple("ancho", 15),
                                                                Field.simple("largo", 20)
                                                        ))
                                                .item(
                                                        Field.simple("_id", 3),
                                                        Field.simple("producto", "libro"),
                                                        Field.simple("autor", "Becquer"),
                                                        Field.simple("precio", 15),
                                                        Field.array("ediciones", 2010, 2011))
                                            .endInsert()
                                    .endQuery()
                                .build();


        // READ EXAMPLE
        Operation read = taskBuilder.createQueryOn("test", "test")
                                        .readItems()
                                            .where(
                                                    Condition.and(
                                                            Condition.element("producto", EQUAL, "libro"),
                                                            Condition.or(
                                                                    Condition.element("autor", EQUAL, "Cervantes"),
                                                                    Condition.element("precio", GREATER, 0)
                                                            )
                                                    )
                                            )
                                        .endRead()
                                .endQuery()
                            .build();

        // DELETE EXAMPLE
        Operation delete = taskBuilder.createQueryOn("test", "test")
                                    .deleteItems()
                                        .where(
                                                Condition.or(
                                                        Condition.or(
                                                                Condition.element("producto", EQUAL, "libro"),
                                                                Condition.element("producto", EQUAL, "audiolibro")
                                                        ),
                                                        Condition.element("ediciones", EQUAL, 2009)
                                                )
                                        )
                                    .endRead()
                                .endQuery()
                            .build();

        // UPDATE EXAMPLE
        Operation update = taskBuilder.createQueryOn("test", "test")
                                    .updateItems()
                                        .withFields(
                                            Field.simple("precio", 100))
                                        .where(
                                                Condition.element("precio", LESSER, 40)
                                        )
                                    .endRead()
                                .endQuery()
                            .build();

        Operation aggregation = taskBuilder.createQueryOn("test", "test")
                                        .aggregation()
                                            .match(Condition.element("precio", GREATER, 20))
                                            .group("$autor", Field.Grouping.group("total", Field.simple(Group.Operation.SUM.getMongoString(), "$precio")))
                                            .sort(Order.create("total", Order.Sort.ASCENDING))
                                            //.projection(
                                            //        Field.Projection.include("autor"),
                                            //        Field.Projection.include("precio")
                                            //)
                                            .endAggregation()
                                        .endQuery()
                                    .build();

        Operation mapReduce = taskBuilder.createQueryOn("test", "test")
                                        .mapReduce()
                                            .map("emit(this.autor, this.precio);")
                                            .reduce("return Array.sum(values);")
                                            .filter(Condition.element("precio", GREATER, 15))
                                        .endMapReduce()
                                    .endQuery()
                                .build();

        MongoExecutor mongoExecutor = Executor.getMongoExecutor("localhost");

        mongoExecutor.doStatement(delete);

        mongoExecutor.doStatement(insert);

        System.out.println("READ: ");
        mongoExecutor.doStatement(read);
        System.out.println();

        System.out.println("AGGREGATION: ");
        mongoExecutor.doStatement(aggregation);
        System.out.println();

        System.out.println("MAP REDUCE: ");
        mongoExecutor.doStatement(mapReduce);
        System.out.println();

        mongoExecutor.doStatement(update);
        mongoExecutor.doStatement(delete);

        System.out.println(mongoExecutor.getString(read));
        System.out.println(mongoExecutor.getString(insert));
        System.out.println(mongoExecutor.getString(update));
        System.out.println(mongoExecutor.getString(delete));
        System.out.println(mongoExecutor.getString(aggregation));
        System.out.println(mongoExecutor.getString(mapReduce));
    }
}
