package executor;

import com.mongodb.MongoClient;
import com.mongodb.client.MapReduceIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import model.field.CompoundCondition;
import model.field.*;
import model.operations.*;
import org.bson.Document;

import java.util.*;

public class MongoExecutor {
    private static MongoExecutor instance;

    private String url;
    private String db;
    private String collection;

    static MongoExecutor getInstance() {
        if (instance == null) {
            instance = new MongoExecutor();
        }
        return instance;
    }

    private MongoExecutor() {
        this.url = "";
        this.db = "";
        this.collection = "";
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public void doStatement(Operation operation) {
        this.db = operation.getDbName();
        this.collection = operation.getCollectionName();

        switch (operation.getType()) {
            case READ:
                read((Read) operation);
                break;
            case INSERT:
                insert((Insert) operation);
                break;
            case UPDATE:
                update((Update) operation);
                break;
            case DELETE:
                delete((Delete) operation);
                break;
            case AGGREGATION:
                aggregation((Aggregation) operation);
                break;
            case MAP_REDUCE:
                mapReduce((MapReduce) operation);
                break;
        }
    }

    public String getString(Operation operation) {
        this.db = operation.getDbName();
        this.collection = operation.getCollectionName();

        switch (operation.getType()) {
            case READ:
                return getReadString((Read) operation);
            case INSERT:
                return getInsertString((Insert) operation);
            case UPDATE:
                return getUpdateString((Update) operation);
            case DELETE:
                return getDeleteString((Delete) operation);
            case AGGREGATION:
                return getAggregationString((Aggregation) operation);
            case MAP_REDUCE:
                return getMapReduceString((MapReduce) operation);
            default:
                return "";
        }
    }

    private String getReadString(Read operation) {
        StringBuilder result = new StringBuilder("db." + operation.getCollectionName() + ".find(");
        Document filter = toDocumentFromCondition(operation.getReadConditions());
        result.append(filter.toJson());
        result.append(")");
        return result.toString();
    }

    private String getInsertString(Insert operation) {
        StringBuilder result = new StringBuilder("db." + operation.getCollectionName() + ".insertMany([");
        List<Document> docs = getDocumentListFromItemList(operation.getElements());
        Iterator<Document> it = docs.iterator();
        while (it.hasNext()) {
            Document d = it.next();
            result.append(d.toJson());
            if (it.hasNext()) {
                result.append(",");
            }
        }
        result.append("])");
        return result.toString();
    }

    private String getUpdateString(Update operation) {
        StringBuilder result = new StringBuilder("db." + operation.getCollectionName() + ".updateMany(");
        Document filter = toDocumentFromCondition(operation.getUpdateConditions());
        Document toUpdate = new Document("$set", getDocumentFromItem(operation.getUpdate()));
        result.append(filter.toJson()).append(",").append(toUpdate.toJson()).append(")");
        return result.toString();
    }

    private String getDeleteString(Delete operation) {
        StringBuilder result = new StringBuilder("db." + operation.getCollectionName() + ".remove(");
        Document filter = toDocumentFromCondition(operation.getDeleteConditions());
        result.append(filter.toJson()).append(")");
        return result.toString();
    }

    private String getAggregationString(Aggregation operation) {
        StringBuilder result = new StringBuilder("db." + operation.getCollectionName() + ".aggregate([");
        List<Document> aggregationParameters = toDocumentFromAggregation(operation);
        Iterator<Document> it = aggregationParameters.iterator();
        while (it.hasNext()) {
            Document d = it.next();
            result.append(d.toJson());
            if (it.hasNext()) {
                result.append(",");
            }
        }
        result.append("])");
        return result.toString();
    }

    private String getMapReduceString(MapReduce operation) {
        StringBuilder result = new StringBuilder("db.").append(operation.getCollectionName()).append(".mapReduce(");
        String map = "function() {" + operation.getRawMapFunction() + "}";
        String reduce = "function(key, values) {" + operation.getRawReduceFunction() + "}";
        Document filter = toDocumentFromCondition(operation.getQuery());

        result.append(map).append(", ");
        result.append(reduce).append(", ");
        result.append("{").append("query").append(":").append(filter.toJson()).append(", ");
        result.append("out").append(":").append("{ inline:1 }");
        result.append("})");
        return result.toString();
    }

    private void read(Read query) {
        MongoClient mongoClient = new MongoClient(url);
        MongoCollection<Document> collection = initMongo(mongoClient);
        Document document = toDocumentFromCondition(query.getReadConditions());
        try (MongoCursor<Document> cursor = collection.find(document).iterator()) {
            while (cursor.hasNext()) {
                System.out.println(cursor.next().toJson());
            }
        }
        mongoClient.close();
    }

    private void insert(Insert query) {
        MongoClient mongoClient = new MongoClient(url);
        MongoCollection<Document> collection = initMongo(mongoClient);
        List<Document> documents = getDocumentListFromItemList(query.getElements());
        collection.insertMany(documents);
        mongoClient.close();
    }

    private void update(Update query) {
        MongoClient mongoClient = new MongoClient(url);
        MongoCollection<Document> collection = initMongo(mongoClient);
        Document filter = toDocumentFromCondition(query.getUpdateConditions());
        Document toUpdate = getDocumentFromItem(query.getUpdate());
        collection.updateMany(filter, new Document("$set", toUpdate));
        mongoClient.close();
    }

    private void delete(Delete query) {
        MongoClient mongoClient = new MongoClient(url);
        MongoCollection<Document> collection = initMongo(mongoClient);
        Document toDelete = toDocumentFromCondition(query.getDeleteConditions());
        collection.deleteMany(toDelete);
        mongoClient.close();
    }

    private void aggregation(Aggregation aggregation) {
        MongoClient mongoClient = new MongoClient(url);
        MongoCollection<Document> collection = initMongo(mongoClient);
        List<Document> parameters = toDocumentFromAggregation(aggregation);
        try (MongoCursor<Document> iterator = collection.aggregate(parameters).iterator()) {
            while (iterator.hasNext()) {
                System.out.println(iterator.next().toJson());
            }
        }
        mongoClient.close();
    }

    private void mapReduce(MapReduce mapReduce) {
        MongoClient mongoClient = new MongoClient(url);
        MongoCollection<Document> collection = initMongo(mongoClient);
        String map = "function() {" + mapReduce.getRawMapFunction() + "}";
        String reduce = "function(key, values) {" + mapReduce.getRawReduceFunction() + "}";
        Document filter = toDocumentFromCondition(mapReduce.getQuery());

        MapReduceIterable<Document> result = collection.mapReduce(map, reduce);
        result.filter(filter);

        try (MongoCursor<Document> iterator = result.iterator()) {
            while (iterator.hasNext()) {
                System.out.println(iterator.next().toJson());
            }
        }
        mongoClient.close();
    }

    private MongoCollection<Document> initMongo(MongoClient mongoClient) {
        MongoDatabase db = mongoClient.getDatabase(this.db);
        return db.getCollection(this.collection);
    }

    private List<Document> getDocumentListFromItemList(List<List<Field>> itemList) {
        List<Document> documentList = new LinkedList<>();
        for (List<Field> item : itemList) {
            documentList.add(getDocumentFromItem(item));
        }

        return documentList;
    }

    private Document getDocumentFromItem(List<Field> item) {
        Document document = new Document();
        for (Field field : item) {
            if (field instanceof CompoundField) {
                document.append(field.getKey(), getDocumentFromCompoundField((CompoundField) field));
            } else {
                document.append(field.getKey(), field.getValue());
            }
        }

        return document;
    }

    private Document getDocumentFromSimpleField(SimpleField simpleField) {
        return new Document(simpleField.getKey(), simpleField.getValue());
    }

    private Document getDocumentFromCompoundField(CompoundField compoundField) {
        Document document = new Document();
        for (Field field : compoundField.getValue()) {
            if (field instanceof CompoundField) {
                document.append(field.getKey(), getDocumentFromCompoundField((CompoundField) field));
            } else {
                document.append(field.getKey(), field.getValue());
            }
        }

        return document;
    }

    private Document toDocumentFromCondition(Condition condition) {
        Document result = new Document();

        if (condition instanceof CompoundCondition) {
            CompoundCondition compoundCondition = (CompoundCondition) condition;
            if (compoundCondition.isSingle()) {
                result = toDocumentFromCondition(compoundCondition.getLeftSide());
            } else {
                CompoundCondition.Operand operand = compoundCondition.getOperand();
                Document left = toDocumentFromCondition(compoundCondition.getLeftSide());
                Document right = toDocumentFromCondition(compoundCondition.getRightSide());
                List<Document> append = new LinkedList<>(Arrays.asList(left, right));
                result.put(operand.getMongoString(), append);
            }
        } else {
            SimpleCondition simpleCondition = (SimpleCondition) condition;
            Condition.Types type = simpleCondition.getCondition().getConditional();
            Document conditionalDoc = new Document();
            conditionalDoc.put(type.getMongoString(), simpleCondition.getCondition().getValue());
            result.append(simpleCondition.getCondition().getKey(), conditionalDoc);
        }
        return result;
    }

    private List<Document> toDocumentFromAggregation(Aggregation operation) {
        List<Document> documentList = new LinkedList<>();
        for (Map.Entry<Aggregation.PipelineKey, Object> action : operation.getPipeline()) {
            Document document = new Document();
            switch (action.getKey()) {
                case MATCH:
                    document.put("$match", toDocumentFromCondition((Condition) action.getValue()));
                    break;
                case SORT:
                    document.put("$sort", getDocumentFromOrder((List<Order>) action.getValue()));
                    break;
                case PROJECTION:
                    document.put("$project", getDocumentFromProjection((Map<String, Integer>) action.getValue()));
                    break;
                case GROUP:
                    document.put("$group", getDocumentFromGroup((AbstractMap.SimpleEntry<String, List<Group>>) action.getValue()));
                    break;
                case COUNT:
                    document.put("$count", action.getValue());
                    break;
                case LIMIT:
                    document.put("$limit", action.getValue());
                    break;
            }
            documentList.add(document);
        }

        return documentList;
    }

    private Document getDocumentFromProjection(Map<String, Integer> projection) {
        Document document = new Document();
        for (Map.Entry<String, Integer> field : projection.entrySet()) {
            document.append(field.getKey(), field.getValue());
        }

        return document;
    }

    private Document getDocumentFromOrder(List<Order> sorting) {
        Document document = new Document();
        for (Order o : sorting) {
            document.append(o.getKey(), o.getSorting().getOrder());
        }

        return document;
    }

    private Document getDocumentFromGroup(Map.Entry<String, List<Group>> grouping) {
        Document document = new Document();
        document.put("_id", grouping.getKey());
        for (Group group : grouping.getValue()) {
            if (group.getExpression() instanceof SimpleField) {
                document.put(group.getField(), getDocumentFromSimpleField((SimpleField) group.getExpression()));
            } else {
                document.put(group.getField(), getDocumentFromCompoundField((CompoundField) group.getExpression()));
            }
        }

        return document;
    }
}
