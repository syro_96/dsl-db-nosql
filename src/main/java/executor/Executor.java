package executor;

public class Executor {
    private Executor() { }

    public static MongoExecutor getMongoExecutor(String url) {
        MongoExecutor mongoExecutor = MongoExecutor.getInstance();
        mongoExecutor.setUrl(url);
        mongoExecutor.setDb("");
        mongoExecutor.setCollection("");
        return mongoExecutor;
    }
}
