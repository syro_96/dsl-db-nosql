package builder;

import model.field.CompoundCondition;
import model.operations.Read;

public class ReadBuilder {
    private QueryBuilder queryBuilder;
    private Read read;

    ReadBuilder(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
        this.read = new Read();
    }

    public ReadBuilder where(CompoundCondition compoundCondition) {
        this.read.setReadConditions(compoundCondition);
        return this;
    }

    public QueryBuilder endRead() {
        this.queryBuilder.setOperation(read);
        return this.queryBuilder;
    }
}
