package builder;

import model.field.CompoundCondition;
import model.field.Group;
import model.field.Order;
import model.operations.Aggregation;

import java.util.*;

public class AggregationBuilder {
    private QueryBuilder queryBuilder;
    private Aggregation aggregation;

    AggregationBuilder(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
        this.aggregation = new Aggregation();
    }

    public AggregationBuilder match(CompoundCondition match) {
        this.aggregation.getPipeline().add(new AbstractMap.SimpleEntry<>(Aggregation.PipelineKey.MATCH, match));
        return this;
    }

    public AggregationBuilder sort(Order... orders) {
        this.aggregation.getPipeline().add(new AbstractMap.SimpleEntry<>(Aggregation.PipelineKey.SORT, new LinkedList<>(Arrays.asList(orders))));
        return this;
    }

    public AggregationBuilder group(String autor, Group... group) {
        this.aggregation.getPipeline().add(new AbstractMap.SimpleEntry<>(Aggregation.PipelineKey.GROUP, new AbstractMap.SimpleEntry<>(autor, Arrays.asList(group))));
        return this;
    }

    public final AggregationBuilder projection(AbstractMap.SimpleEntry<String, Integer>... projection) {
        Map<String, Integer> projectionMap = new HashMap<>();
        for (Map.Entry<String, Integer> pair : projection) {
            projectionMap.put(pair.getKey(), pair.getValue());
        }

        this.aggregation.getPipeline().add(new AbstractMap.SimpleEntry<>(Aggregation.PipelineKey.PROJECTION, projectionMap));
        return this;
    }

    public AggregationBuilder limit(int limit) {
        this.aggregation.getPipeline().add(new AbstractMap.SimpleEntry<>(Aggregation.PipelineKey.LIMIT, limit));
        return this;
    }

    public AggregationBuilder count(String name) {
        this.aggregation.getPipeline().add(new AbstractMap.SimpleEntry<>(Aggregation.PipelineKey.COUNT, name));
        return this;
    }

    public QueryBuilder endAggregation() {
        this.queryBuilder.setOperation(this.aggregation);
        return this.queryBuilder;
    }
}
