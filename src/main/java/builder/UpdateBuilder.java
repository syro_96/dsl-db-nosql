package builder;

import model.field.CompoundCondition;
import model.field.Field;
import model.operations.Update;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class UpdateBuilder {
    private QueryBuilder queryBuilder;
    private Update update;

    UpdateBuilder(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
        this.update = new Update();
    }

    public UpdateBuilder where(CompoundCondition compoundCondition) {
        this.update.setUpdateConditions(compoundCondition);
        return this;
    }

    public UpdateBuilder withFields(Field...fields) {
        this.update.getUpdate().addAll(Arrays.asList(fields));
        return this;
    }

    public QueryBuilder endRead() {
        this.queryBuilder.setOperation(update);
        return this.queryBuilder;
    }
}
