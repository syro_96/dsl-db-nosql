package builder;

import model.field.CompoundCondition;
import model.operations.Delete;

public class DeleteBuilder {
    private QueryBuilder queryBuilder;
    private Delete delete;

    DeleteBuilder(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
        this.delete = new Delete();
    }

    public DeleteBuilder where(CompoundCondition compoundCondition) {
        this.delete.setDeleteConditions(compoundCondition);
        return this;
    }

    public QueryBuilder endRead() {
        this.queryBuilder.setOperation(delete);
        return this.queryBuilder;
    }
}
