package builder;

import model.field.Field;
import model.operations.Insert;

import java.util.Arrays;
import java.util.LinkedList;

public class InsertBuilder {
    private QueryBuilder queryBuilder;
    private Insert insert;

    InsertBuilder(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
        this.insert = new Insert();
    }

    public InsertBuilder item(Field...fields) {
        this.insert.getElements().add(new LinkedList<>(Arrays.asList(fields)));
        return this;
    }

    public QueryBuilder endInsert() {
        this.queryBuilder.setOperation(insert);
        return this.queryBuilder;
    }
}
