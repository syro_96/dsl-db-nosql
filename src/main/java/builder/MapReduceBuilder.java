package builder;

import model.field.CompoundCondition;
import model.operations.MapReduce;

public class MapReduceBuilder {
    private QueryBuilder queryBuilder;
    private MapReduce mapReduce;

    MapReduceBuilder(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
        this.mapReduce = new MapReduce();
    }

    public MapReduceBuilder map(String mapFunction) {
        this.mapReduce.setRawMapFunction(mapFunction);
        return this;
    }

    public MapReduceBuilder reduce(String reduceFunction) {
        this.mapReduce.setRawReduceFunction(reduceFunction);
        return this;
    }

    public MapReduceBuilder filter(CompoundCondition query) {
        this.mapReduce.setQuery(query);
        return this;
    }

    public QueryBuilder endMapReduce() {
        this.queryBuilder.setOperation(mapReduce);
        return queryBuilder;
    }

}
