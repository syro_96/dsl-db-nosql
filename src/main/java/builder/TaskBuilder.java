package builder;

import model.operations.Operation;

/**
 * Builder publico y principal. A partir de este builder se irán encadenando el resto de builders
 * y asi poder formar un flujo fluido e intuitivo.
 */
public class TaskBuilder {
    private Operation statement;

    public TaskBuilder() {
        this.statement = null;
    }

    Operation getStatement() {
        return statement;
    }

    void setStatement(Operation statement) {
        this.statement = statement;
    }

    /**
     * Método para construir una query
     * @param dbName String que representa el nombre de la DB
     * @param collectionName String que representa el nombre de la coleccion o tabla
     * @return QueryBuilder que tendrá los metodos para seguir interactuando
     */
    public QueryBuilder createQueryOn(String dbName, String collectionName) {
        return new QueryBuilder(dbName, collectionName, this);
    }

    /**
     * Método final para construir el statement que se usará para la ejecución con la DB
     * @return Operation
     */
    public Operation build() {
        return this.statement;
    }
}
