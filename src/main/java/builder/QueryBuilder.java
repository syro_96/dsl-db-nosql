package builder;

import model.operations.*;

public class QueryBuilder {
    private TaskBuilder taskBuilder;

    private Operation operation;
    private String dbName;
    private String collection;

    QueryBuilder(String dbName, String collection, TaskBuilder taskBuilder) {
        this.dbName = dbName;
        this.collection = collection;
        this.taskBuilder = taskBuilder;
    }

    TaskBuilder getTaskBuilder() {
        return this.taskBuilder;
    }

    void setOperation(Operation operation) {
        this.operation = operation;
    }

    Operation getOperation() {
        return this.operation;
    }

    /**
     * Metodo fluent para la operacion READ
     * @return ReadBuilder para seguir interactuando con la operacion READ
     */
    public ReadBuilder readItems() {
        this.operation = new Read();
        return new ReadBuilder(this);
    }

    /**
     * Metodo fluent para la operacion INSERT
     * @return ReadBuilder para seguir interactuando con la operacion INSERT
     */
    public InsertBuilder insertItems() {
        this.operation = new Insert();
        return new InsertBuilder(this);
    }

    /**
     * Metodo fluent para la operacion DELETE
     * @return ReadBuilder para seguir interactuando con la operacion DELETE
     */
    public DeleteBuilder deleteItems() {
        this.operation = new Delete();
        return new DeleteBuilder(this);
    }

    /**
     * Metodo fluent para la operacion UPDATE
     * @return ReadBuilder para seguir interactuando con la operacion UPDATE
     */
    public UpdateBuilder updateItems() {
        this.operation = new Update();
        return new UpdateBuilder(this);
    }

    /**
     * Metodo fluent para la operacion de AGGREGATION
     * @return AggregationBuilder para seguir interactuando con la operacion AGGREGATION
     */
    public AggregationBuilder aggregation() {
        this.operation = new Aggregation();
        return new AggregationBuilder(this);
    }

    /**
     * Metodo fluent para la operacion de MAP REDUCE
     * @return MapReduceBuilder para seguir interactuando con la operacion MAP REDUCE
     */
    public MapReduceBuilder mapReduce() {
        this.operation = new MapReduce();
        return new MapReduceBuilder(this);
    }

    public TaskBuilder endQuery() {
        this.operation.setDbName(dbName);
        this.operation.setCollectionName(collection);
        this.taskBuilder.setStatement(operation);
        return taskBuilder;
    }
}
