package model.operations;

import model.field.CompoundCondition;
import model.field.Field;

import java.util.LinkedList;
import java.util.List;

public class Update extends Operation {
    private List<Field> update;
    private CompoundCondition updateConditions;

    public Update() {
        super(Types.UPDATE);
        this.update = new LinkedList<>();
        this.updateConditions = new CompoundCondition();
    }

    public List<Field> getUpdate() {
        return update;
    }

    public void setUpdate(List<Field> update) {
        this.update = update;
    }

    public CompoundCondition getUpdateConditions() {
        return updateConditions;
    }

    public void setUpdateConditions(CompoundCondition updateConditions) {
        this.updateConditions = updateConditions;
    }
}
