package model.operations;

import model.field.CompoundCondition;

public class Read extends Operation {
    CompoundCondition readConditions;

    public Read() {
        super(Types.READ);
        this.readConditions = new CompoundCondition();
    }

    public CompoundCondition getReadConditions() {
        return readConditions;
    }

    public void setReadConditions(CompoundCondition readConditions) {
        this.readConditions = readConditions;
    }
}
