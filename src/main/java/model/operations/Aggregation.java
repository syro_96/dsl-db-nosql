package model.operations;

import java.util.*;

public class Aggregation extends Operation {
    public enum PipelineKey {
        MATCH,
        SORT,
        PROJECTION,
        GROUP,
        COUNT,
        LIMIT,
        ;
    }

    private List<Map.Entry<PipelineKey, Object>> pipeline;

    public Aggregation() {
        super(Types.AGGREGATION);
        this.pipeline = new LinkedList<>();
    }

    public List<Map.Entry<PipelineKey, Object>> getPipeline() {
        return pipeline;
    }

    public void setPipeline(List<Map.Entry<PipelineKey, Object>> pipeline) {
        this.pipeline = pipeline;
    }
}
