package model.operations;

import model.field.Field;

import java.util.LinkedList;
import java.util.List;

public class Insert extends Operation {
    private List<List<Field>> elements;

    public Insert() {
        super(Types.INSERT);
        this.elements = new LinkedList<>();
    }

    public List<List<Field>> getElements() {
        return elements;
    }

    public void setElements(List<List<Field>> elements) {
        this.elements = elements;
    }
}
