package model.operations;

import model.field.CompoundCondition;

public class Delete extends Operation {
    private CompoundCondition deleteConditions;

    public Delete() {
        super(Types.DELETE);
        this.deleteConditions = new CompoundCondition();
    }

    public CompoundCondition getDeleteConditions() {
        return deleteConditions;
    }

    public void setDeleteConditions(CompoundCondition deleteConditions) {
        this.deleteConditions = deleteConditions;
    }
}
