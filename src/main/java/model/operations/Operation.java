package model.operations;

public abstract class Operation {
    public enum Types {
        READ,
        INSERT,
        DELETE,
        UPDATE,
        AGGREGATION,
        MAP_REDUCE,
        ;
    }

    private String dbName;
    private String collectionName;
    private Types type;

    Operation(Types type) {
        this.dbName = "";
        this.collectionName = "";
        this.type = type;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public Types getType() {
        return type;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }
}
