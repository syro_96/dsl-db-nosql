package model.operations;

import model.field.CompoundCondition;

public class MapReduce extends Operation {
    private String rawMapFunction;
    private String rawReduceFunction;
    private CompoundCondition query;

    public MapReduce() {
        super(Types.MAP_REDUCE);
    }

    public String getRawMapFunction() {
        return rawMapFunction;
    }

    public void setRawMapFunction(String rawMapFunction) {
        this.rawMapFunction = rawMapFunction;
    }

    public String getRawReduceFunction() {
        return rawReduceFunction;
    }

    public void setRawReduceFunction(String rawReduceFunction) {
        this.rawReduceFunction = rawReduceFunction;
    }

    public CompoundCondition getQuery() {
        return query;
    }

    public void setQuery(CompoundCondition query) {
        this.query = query;
    }
}
