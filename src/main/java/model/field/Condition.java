package model.field;

import static model.field.CompoundCondition.Operand.AND;
import static model.field.CompoundCondition.Operand.OR;

public interface Condition {
    enum Types {
        EQUAL("$eq"),
        NOT_EQUAL("$ne"),
        GREATER("$gt"),
        LESSER("$lt"),
        GREATER_EQUAL("$gte"),
        LESSER_EQUAL("$lte"),
        ;

        private String mongoString;

        Types(String mongoString) {
            this.mongoString = mongoString;
        }

        public String getMongoString() {
            return mongoString;
        }
    }

    static ConditionalField field(String key, Types conditional, String value) {
        return new ConditionalField(key, value, conditional);
    }

    static ConditionalField field(String key, Types conditional, Integer value) {
        return new ConditionalField(key, value, conditional);
    }

    static ConditionalField field(String key, Types conditional, Double value) {
        return new ConditionalField(key, value, conditional);
    }

    static ConditionalField field(String key, Types conditional, Boolean value) {
        return new ConditionalField(key, value, conditional);
    }

    static CompoundCondition element(String key, Types conditional, String value) {
        return new CompoundCondition(new SimpleCondition(field(key, conditional, value)));
    }

    static CompoundCondition element(String key, Types conditional, Integer value) {
        return new CompoundCondition(new SimpleCondition(field(key, conditional, value)));
    }

    static CompoundCondition element(String key, Types conditional, Double value) {
        return new CompoundCondition(new SimpleCondition(field(key, conditional, value)));
    }

    static CompoundCondition element(String key, Types conditional, Boolean value) {
        return new CompoundCondition(new SimpleCondition(field(key, conditional, value)));
    }

    static CompoundCondition and(CompoundCondition left, CompoundCondition right) {
        return new CompoundCondition(left, AND, right);
    }

    static CompoundCondition or(CompoundCondition left, CompoundCondition right) {
        return new CompoundCondition(left, OR, right);
    }
}
