package model.field;

public class Group {
    public enum Operation {
        SUM("$sum"),
        MIN("$min"),
        MAX("$max"),
        AVERANGE("$avg"),
        ;

        private String mongoString;

        Operation(String mongoString) {
            this.mongoString = mongoString;
        }

        public String getMongoString() {
            return this.mongoString;
        }
    }

    private String field;
    private Field expression;

    Group(String field, Field expression) {
        this.field = field;
        this.expression = expression;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Field getExpression() {
        return expression;
    }

    public void setExpression(Field expression) {
        this.expression = expression;
    }
}
