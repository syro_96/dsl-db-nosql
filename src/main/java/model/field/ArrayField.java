package model.field;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ArrayField extends Field {
    protected List<Object> value;

    protected ArrayField(String key, String...value) {
        this(key, new LinkedList<>(Arrays.asList(value)));
    }

    protected ArrayField(String key, Integer...value) {
        this(key, new LinkedList<>(Arrays.asList(value)));
    }

    protected ArrayField(String key, Double...value) {
        this(key, new LinkedList<>(Arrays.asList(value)));
    }

    protected ArrayField(String key, Boolean...value) {
        this(key, new LinkedList<>(Arrays.asList(value)));
    }

    private ArrayField(String key, List<Object> value) {
        super(key);
        this.value = value;
    }

    @Override
    public List<Object> getValue() {
        return this.value;
    }
}
