package model.field;

public class SimpleCondition implements Condition {
    private ConditionalField condition;

    protected SimpleCondition(ConditionalField condition) {
        this.condition = condition;
    }

    public ConditionalField getCondition() {
        return condition;
    }

    public void setCondition(ConditionalField condition) {
        this.condition = condition;
    }
}
