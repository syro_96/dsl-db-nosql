package model.field;

public class ConditionalField extends SimpleField {
    private CompoundCondition.Types conditional;

    ConditionalField(String key, Object value, CompoundCondition.Types conditional) {
        super(key, value);
        this.conditional = conditional;
    }

    public CompoundCondition.Types getConditional() {
        return conditional;
    }
}
