package model.field;

import java.util.AbstractMap;
import java.util.Map;

public abstract class Field {
    protected String key;

    protected Field(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public abstract Object getValue();

    public static SimpleField simple(String key, String value) {
        return new SimpleField(key, value);
    }

    public static SimpleField simple(String key, Integer value) {
        return new SimpleField(key, value);
    }

    public static SimpleField simple(String key, Double value) {
        return new SimpleField(key, value);
    }

    public static SimpleField simple(String key, Boolean value) {
        return new SimpleField(key, value);
    }

    public static ArrayField array(String key, String... value) {
        return new ArrayField(key, value);
    }

    public static ArrayField array(String key, Integer... value) {
        return new ArrayField(key, value);
    }

    public static ArrayField array(String key, Double... value) {
        return new ArrayField(key, value);
    }

    public static ArrayField array(String key, Boolean... value) {
        return new ArrayField(key, value);
    }

    public static CompoundField compound(String key, Field... value) {
        return new CompoundField(key, value);
    }

    public static class Projection {
        public static Map.Entry<String, Integer> exclude(String key) {
            return new AbstractMap.SimpleEntry<>(key, 0);
        }

        public static Map.Entry<String, Integer> include(String key) {
            return new AbstractMap.SimpleEntry<>(key, 1);
        }
    }

    public static class Grouping {
        public static Group group(String field, SimpleField expression) {
            return new Group(field, expression);
        }

        public static Group group(String field, CompoundField expression) {
            return new Group(field, expression);
        }
    }
}
