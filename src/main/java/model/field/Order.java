package model.field;

public class Order {
    public enum Sort {
        ASCENDING(1),
        DESCENDING(-1),
        ;

        private Integer order;

        Sort(Integer order) {
            this.order = order;
        }

        public Integer getOrder() {
            return order;
        }
    }

    private String key;
    private Sort sorting;

    Order(String key, Sort sorting) {
        this.key = key;
        this.sorting = sorting;
    }

    public String getKey() {
        return key;
    }

    public Sort getSorting() {
        return sorting;
    }

    public static Order create(String key, Sort sorting) {
        return new Order(key, sorting);
    }
}
