package model.field;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class CompoundField extends Field {
    private List<Field> value;

    protected CompoundField(String key, Field...field) {
        super(key);
        this.value = new LinkedList<>(Arrays.asList(field));
    }

    @Override
    public List<Field> getValue() {
        return this.value;
    }
}
