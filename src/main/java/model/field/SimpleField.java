package model.field;

public class SimpleField extends Field {
    protected Object value;

    SimpleField(String key, Object value) {
        super(key);
        this.value = value;
    }

    @Override
    public Object getValue() {
        return this.value;
    }
}
