package model.field;

public class CompoundCondition implements Condition {
    public enum Operand {
        AND("$and"),
        OR("$or"),
        ;

        private String mongoString;

        Operand(String mongoString) {
            this.mongoString = mongoString;
        }

        public String getMongoString() {
            return mongoString;
        }
    }

    private Condition leftSide;
    private Operand operand;
    private Condition rightSide;

    public CompoundCondition() { }

    protected CompoundCondition(Condition leftSide) {
        this.leftSide = leftSide;
        this.operand = null;
        this.rightSide = null;
    }

    protected CompoundCondition(Condition leftSide, Operand operand, Condition rightSide) {
        this.leftSide = leftSide;
        this.operand = operand;
        this.rightSide = rightSide;
    }

    public Condition getLeftSide() {
        return leftSide;
    }

    public Operand getOperand() {
        return operand;
    }

    public Condition getRightSide() {
        return rightSide;
    }

    public boolean isSingle() {
        return (this.operand == null);
    }
}
